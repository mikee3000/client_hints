from typing import Optional
import json
from fastapi import FastAPI, Header, Request
from datetime import datetime as dt

app = FastAPI()

def timestamp():
    return dt.strftime(dt.now(), '%Y-%m-%d_T_%H:%M:%S')

def log(d):
    j = json.dumps(d)
    with open('app/logs.json', 'a') as l:
        l.write(f"{j}\n")
    return 200

def tabulate(d):
    cur_time = timestamp()
    d["timestamp"] = cur_time
    j = json.dumps(d)
    with open('app/logs.json', 'a') as l:
        l.write(f"{j}\n")
    table = "<table>"
    for k, v in d.items():
        table += f"<tr><th>{k}</th><td>{v}</td></tr>"
    table += "</table>"
    return table

@app.post("/post_headers")
async def post_headers(request: Request):
    s = '\n'.join(str(i) for i in request.headers.items())
    return s

@app.get("/log_message/{message}")
def log_message(message: str, request: Request):
    cur_time = timestamp()
    d = {"timestamp": cur_time, "message": message, **request.headers}
    print(f'message {d}')
    return log(str(d))

@app.get("/api")
async def read_items(
        request: Request,
        user_agent: Optional[str] = Header(None),
        sec_ch_ua_model: Optional[str] = Header(None), 
        sec_ch_ua_full_version_list: Optional[str] = Header(None),
        sec_ch_ua_full_version: Optional[str] = Header(None),
        sec_ch_ua: Optional[str] = Header(None),
        sec_ch_ua_mobile: Optional[str] = Header(None),
        sec_ch_ua_platform: Optional[str] = Header(None),
        sec_ch_ua_platform_version: Optional[str] = Header(None),
        sec_ch_ua_arch: Optional[str] = Header(None),
        sec_ch_ua_bitness: Optional[str] = Header(None),
        sec_ch_ua_wow64: Optional[str] = Header(None),
        content_dpr: Optional[str] = Header(None),
        sec_ch_content_dpr: Optional[str] = Header(None),
        device_memory: Optional[str] = Header(None),
        sec_ch_device_memory: Optional[str] = Header(None),
        dpr: Optional[str] = Header(None),
        sec_ch_dpr: Optional[str] = Header(None),
        viewport_width: Optional[str] = Header(None),
        sec_ch_viewport_width: Optional[str] = Header(None),
        width: Optional[str] = Header(None),
        sec_ch_width: Optional[str] = Header(None),
        downlink: Optional[str] = Header(None),
        sec_ch_downlink: Optional[str] = Header(None),
        ect: Optional[str] = Header(None),
        sec_ch_ect: Optional[str] = Header(None),
        rtt: Optional[str] = Header(None),
        sec_ch_rtt: Optional[str] = Header(None),
        save_data: Optional[str] = Header(None),
        sec_ch_save_data: Optional[str] = Header(None),
        sec_ch_viewport_height: Optional[str] = Header(None),
        sec_ch_prefers_color_scheme: Optional[str] = Header(None),
        x_operamini_phone_ua: Optional[str] = Header(None),
        x_requested_with: Optional[str] = Header(None)
        ):
    d = {"user-agent": user_agent,
        "Sec-CH-UA-Model": sec_ch_ua_model,
        "Sec-CH-UA-Full-Version-List": sec_ch_ua_full_version_list,
        "Sec-CH-UA-Full-Version": sec_ch_ua_full_version,
        "Sec-CH-UA": sec_ch_ua,
        "Sec-CH-UA-Mobile": sec_ch_ua_mobile,
        "Sec-CH-UA-Platform": sec_ch_ua_platform,
        "Sec-CH-UA-Platform-Version": sec_ch_ua_platform_version,
        "Sec-CH-UA-Arch": sec_ch_ua_arch,
        "Sec-CH-UA-Bitness": sec_ch_ua_bitness,
        "Sec-CH-UA-WoW64": sec_ch_ua_wow64,
        "Content-DPR": content_dpr,
        "Sec-CH-Content-DPR": sec_ch_content_dpr,
        "Device-Memory": device_memory,
        "Sec-CH-Device-Memory": sec_ch_device_memory,
        "DPR": dpr,
        "Sec-CH-DPR": sec_ch_dpr,
        "Viewport-Width": viewport_width,
        "Sec-CH-Viewport-Width": sec_ch_viewport_width,
        "Width": width,
        "Sec-CH-Width": sec_ch_width,
        "Downlink": downlink,
        "Sec-CH-Downlink": sec_ch_downlink,
        "ECT": ect,
        "Sec-CH-ECT": sec_ch_ect,
        "RTT": rtt,
        "Sec-CH-RTT": sec_ch_rtt,
        "Sec-CH-Save-Data": sec_ch_save_data,
        "Sec-CH-Viewport-Height": sec_ch_viewport_height,
        "Sec-CH-Prefers-Color-Scheme": sec_ch_prefers_color_scheme,
        "x-operamini-phone-ua": x_operamini_phone_ua,
        "X-Requested-With-": x_requested_with
    }
    existing_keys_lower = [k.lower() for k in d.keys()]
    left_over = {h: v for h, v in request.headers.items() if h.lower() not in existing_keys_lower}
    d = {**d, **left_over}
    return tabulate(d)
